
      game = (function init() {

        window.console_on=false;
        window.console_on=true;
        window.console.log_wrapper=window.console.log;
        window.console.log=function(a){
          if (window.console_on)            { window.console.log_wrapper(a);}
        }
        window.color=Color;
        var control_type='absolute';
        var keyListener;
        var globals=window.globals;
        //var elt=globals.element_id_lookup;
        var controller;
        var ctx;
        var turn  = [];
        var dim_to_pix=8; // number of pixels per game square
        var xdim=51,ydim=51; //dimension of game in game squares

        var xpixels= parseInt(xdim*dim_to_pix*1.3);
        var ypixels = parseInt(ydim*dim_to_pix*1.3);
        var canvas_score_height= 70;
        
        var canvas_score=0;        
        var MR = Math.random;
        var X = 5 + (MR() * (xdim - dim_to_pix) )|0; // OR'ing it with 0 returns an integer (in JS)
        var Y = 5 + (MR() * (ydim - dim_to_pix) )|0;
        var queue = [];
        var snake;// = new Snake(X,Y);
        var levelQueue1 ;
        //yup, globals for now
        var renderer, palette;
        var center_on;
        var rotate_board;

        var elements = 1;
        var map = [];
        var score;
        var ctx_score;

        var direction = MR() * 3 | 0;

        var interval = 0;

        var inc_score = 50;

        var sum = 0, easy = 0;

        var i, dir;

        var turn_board = false;
        
        var center_x , center_y;
        
        win = window;
        doc = document;


        var setInt = win.setInterval;
        var clInt = win.clearInterval;
        var setScore=function(s){
          score=s;
          $("#score").html(s);
        }
        
        var snake_length=function(){return snake.queue.length + 1;};
        
        var publishLength=function(){
          $("#snake_length").html("Length: "+snake_length() );
        }
        var trapped=function(){
          //does the snake head have a path to safety ?, ie  , is he blocked by his own body
        }
        //game to screen
        var g_to_s = function(x,y,dir){
          var x_pix = 0 , y_pix = 0;
          if (typeof dir == 'undefined') {
            dir= direction;
          }
          switch(dir){
            case 0:
            break;
            case 1:
            //north
              x_pix = x * dim_to_pix;
              y_pix = y * dim_to_pix;
            break;
            case 2:
            break;
            case 3:
            break;
          };
        }
        var s_to_g=function(x,y,dir){
          if (typeof dir == 'undefined') {
            dir= direction;
          }
          switch(dir){
            case 0:
            break;
            case 1:
            //north

            break;
            case 2:
            break;
            case 3:
            break;
          };
        }

        function draw_info_area(){
          var h=ctx_info.canvas.height;
          var w=ctx_info.canvas.width;
          var ss= "asdfas3eqwrweqasdfinfo";
          ctx_info.font="10pt Arial"
          ctx_info.clearRect(0,0,w,h);
          ctx_score.fillStyle = "#222222" ;
          ctx_info.fillRect(0,0,w,h);
          ctx_info.strokeStyle="#ab23fe";
          ctx_info.strokeText(ss, 10, 120);
          ctx_info.fillStyle="#1a1a1a";
          ctx_info.fillText(ss, 10, 140);

        }

        function reset_game(){
          console.log('reset_game()');
          canvas = doc.createElement('canvas');
          canvas.setAttribute('width', xpixels);
          canvas.setAttribute('height', ypixels );
          window.ctx_main = canvas.getContext('2d');


          canvas_score = doc.createElement('canvas');
          canvas_score.setAttribute('width', xpixels);
          canvas_score.setAttribute('height', canvas_score_height);
          
          canvas_info = doc.createElement('canvas');
          canvas_info.setAttribute('width', 250);
          // canvas_info.setAttribute('height', canvas_score_height + canvas.getAttribute('height'));
          canvas_info.setAttribute('height', 530);
          ctx_info= canvas_info.getContext('2d');
          ctx_score= canvas_score.getContext('2d');
          draw_info_area()


          $("div#game_column div#game_score").html(canvas_score);
          $("div#game_right div#game_info").html(canvas_info);
          $("div#game_column div#game_board").html(canvas);
          $("div#game_column div#game_info").show();

          queue = [];
          // center_on = 'snake_head';
          rotate_board=false;
          center_on = 'snake_box';

          setScore( 0 );
          inc_score = 50;
          center_x=parseInt(xpixels/2.0) , center_y=parseInt( ypixels/2.0 );

          gameMap  = new GameMap(xdim,ydim, dim_to_pix);
          map = gameMap.map;
          snake = new Snake(5 + (MR() * (xdim - dim_to_pix))|0, 5 + (MR() * (ydim - dim_to_pix))|0 );
          snake.direction = MR() * 3 | 0;
          palette=new ColorPalette;
          var render_options={
            palette:palette,
            center_on: center_on,
            rotate_board: rotate_board,
            center_x:center_x,
            center_y:center_y,
            xdim: xdim,
            ydim: ydim,
            xpixels: xpixels,
            ypixels: ypixels,
            dim_to_pix:dim_to_pix
          };
          renderer = new Renderer(ctx_main,gameMap,snake,render_options);
          console.log("renderer.@snake");
          console.log(renderer.snake);

          score = new Score(ctx_score);
          console.log("snake_level_set");
          console.log(snake_level_set);
          controller = new Controller(renderer, snake_level_set, score);
        }
          thudewaker = 0;

        function write_to_scoreboard(s){
          // ctx_score= canvas_score.getContext('2d');
          var h=ctx_score.canvas.height;
          var w=ctx_score.canvas.width;
          if ( s[0] !== "undefined" ){
              ctx_score.clearRect(0,0,w,h);
              ctx_score.fillStyle = "#222222" ;
              ctx_score.fillRect(0,0,w,h);
              ctx_score.fillStyle="#ffffff"
            $.each(s, function(co,item){
              ctx_score.fillText(item, 60, 30+(co*15));
            });
          }
        }

        keyListener = function(e) {
        }
        reset_game();

        // everything here gets published to the game global object, for access from within the coffescript library
        return {
          control_type: control_type,
          controller: controller,
          score: score,
          ctx_score: ctx_score,
          ctx: ctx,
          ctx_info: ctx_info,
          renderer: renderer,
          gameMap: gameMap,
          snake:snake,
          palette:palette,
          reset_game: reset_game,
          publishLength: publishLength,
          write_to_scoreboard: write_to_scoreboard
          // scoreboard_with_function: scoreboard_with_function

        };
      })();

