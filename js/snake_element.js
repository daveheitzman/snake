// Generated by CoffeeScript 1.3.3
(function() {
  var BaseElement, ColorPalette, Farmer, GameMap, Globals, MapDwellers, Mushroom, Mushroom2, Mushroom3, NullElement, Renderer, SnakeBody, element_id_lookup,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  window.elt = element_id_lookup = {
    snake_body: 2,
    2: function() {
      return new SnakeBody;
    },
    mushroom: 15,
    15: function() {
      return new Mushroom;
    },
    mushroom2: 16,
    16: function() {
      return new Mushroom2;
    },
    farmer: 30,
    30: function() {
      return new Farmer;
    },
    0: function() {
      return new NullElement;
    },
    null_element: 0
  };

  MapDwellers = (function() {

    function MapDwellers() {
      this.dwellers = [];
      this.farmers = [];
      this.newFoodRate = 0.04;
      this.farmerSpawnRate = 0.002;
    }

    MapDwellers.prototype.update = function(gameMap, score, ticks, snake) {
      var dw, farmer, map, mush, new_dwellers, sn, _i, _len, _ref;
      new_dwellers = [];
      _ref = this.dwellers;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        dw = _ref[_i];
        if (dw.locx < 0 || dw.locy < 0 || gameMap.map[dw.locx][dw.locy] !== 0) {
          new_dwellers.push(dw);
          dw.update(gameMap.map);
        }
      }
      this.dwellers = new_dwellers;
      if (Math.random() <= this.newFoodRate) {
        mush = gameMap.placeFood(score, ticks, snake);
      }
      if (Math.random() <= this.farmerSpawnRate) {
        farmer = gameMap.placeFarmer(score, ticks, snake);
      }
      if (mush != null) {
        this.dwellers.push(mush);
      }
      if (farmer != null) {
        this.dwellers.push(farmer);
      }
      map = gameMap;
      return sn = game.snake;
    };

    return MapDwellers;

  })();

  GameMap = (function() {

    function GameMap(xdim, ydim, dim_to_pix) {
      var co, co2, _i, _j, _ref, _ref1;
      this.xdim = xdim;
      this.ydim = ydim;
      this.dim_to_pix = dim_to_pix;
      this.map = [];
      for (co = _i = 0, _ref = this.xdim; 0 <= _ref ? _i < _ref : _i > _ref; co = 0 <= _ref ? ++_i : --_i) {
        this.map[co] = [];
        for (co2 = _j = 0, _ref1 = this.ydim; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; co2 = 0 <= _ref1 ? ++_j : --_j) {
          this.map[co][co2] = 0;
        }
      }
      this.mapDwellers = new MapDwellers(this);
    }

    GameMap.prototype.mapGet = function(x, y, dir) {
      var xtmp, ytmp;
      xtmp = x;
      ytmp = y;
      if (x[0]) {
        xtmp = x[0];
        ytmp = x[1];
      }
      x = xtmp;
      y = ytmp;
      if ((0 > x && x > this.xdim - 1) || (0 > y && y > this.ydim - 1)) {
        return null;
      }
      if (dir == null) {
        dir = 1;
      }
      switch (dir) {
        case 0:
          return this.map[y][this.xdim - 1 - x];
        case 1:
          return this.map[x][y];
        case 2:
          return this.map[this.xdim - 1 - x][this.ydim - 1 - y];
        case 3:
          return this.map[this.xdim - 1 - y][x];
      }
    };

    GameMap.prototype.updateMapDwellers = function(score, ticks, snake) {
      return this.mapDwellers.update(this, score, ticks, snake);
    };

    GameMap.prototype.placeFood = function(score, ticks, snake) {
      var re;
      re = this.randomEmpty();
      if (re != null) {
        return this.map[re[0]][re[1]] = new Mushroom(re[0], re[1], score, ticks, snake);
      } else {
        return null;
      }
    };

    GameMap.prototype.placeFarmer = function(score, ticks, snake) {
      var re;
      console.log("GameMap.placeFarmer() ");
      re = this.randomEmpty();
      if (re != null) {
        return this.map[re[0]][re[1]] = new Farmer(re[0], re[1], score, ticks, snake);
      } else {
        return null;
      }
    };

    GameMap.prototype.randomEmpty = function() {
      var MR, failsafe, x, y;
      MR = Math.random;
      x = MR() * this.xdim | 0;
      y = MR() * this.ydim | 0;
      failsafe = 25;
      while (this.map[x][y] !== 0 && failsafe > 0) {
        x = MR() * this.xdim | 0;
        y = MR() * this.ydim | 0;
        failsafe--;
        0;

      }
      if (this.map[x][y] === 0) {
        return [x, y];
      } else {
        return null;
      }
    };

    GameMap.prototype.putSnakeOn = function(oldxy) {
      return this.setSquare(oldxy, new SnakeBody);
    };

    GameMap.prototype.setSquare = function(erase, v) {
      return this.map[erase[0]][erase[1]] = v;
    };

    GameMap.prototype.empties_orth = function(x, y) {
      var empties;
      empties = [];
      if (!(x > -1 && y > -1 && x < this.xdim && y < this.ydim)) {
        return empties;
      }
      if (map[x][y - 1] === 0) {
        empties.push([x, y - 1]);
      }
      if (map[x + 1][y] === 0) {
        empties.push([x + 1, y]);
      }
      if (map[x][y + 1] === 0) {
        empties.push([x, y + 1]);
      }
      if (map[x - 1][y] === 0) {
        empties.push([x - 1, y]);
      }
      return empties;
    };

    GameMap.prototype.empties_diag = function(x, y) {
      var empties;
      empties = [];
      if (!(x > -1 && y > -1 && x < this.xdim && y < this.ydim)) {
        return empties;
      }
      if (map[x - 1][y - 1] === 0) {
        empties.push([x - 1, y - 1]);
      }
      if (map[x + 1][y - 1] === 0) {
        empties.push([x + 1, y - 1]);
      }
      if (map[x + 1][y + 1] === 0) {
        empties.push([x + 1, y + 1]);
      }
      if (map[x - 1][y + 1] === 0) {
        empties.push([x - 1, y + 1]);
      }
      return empties;
    };

    GameMap.prototype.with_wrap = function(x, y) {
      return [(this.xdim + (x % this.xdim)) % this.xdim, (this.ydim + (y % this.ydim)) % this.ydim];
    };

    return GameMap;

  })();

  ColorPalette = (function() {

    function ColorPalette() {
      this.snake_box_bg = "#113311";
      this.snake_box_border = "#337723";
      this.game_board_bg = "#000000";
      this.score_bg = "#000000";
      this.snake_body_color = "#aaddaa";
      this.snake_head_color = "#ccffcc";
      this.snake_head_stroke = "#eeffee";
      this.score = {
        stroke: "#ee2223",
        fill: "#000000"
      };
    }

    return ColorPalette;

  })();

  Renderer = (function() {

    function Renderer(ctx, gameMap, snake, options) {
      var k, _i, _len, _ref;
      this.ctx = ctx;
      this.gameMap = gameMap;
      this.snake = snake;
      this.options = options;
      console.log('Renderer:constructor');
      console.log('ctx:');
      console.log(ctx);
      this.center_on = "snake_box";
      this.dim_to_pix = this.gameMap.dim_to_pix;
      this.xdim = this.gameMap.xdim;
      this.ydim = this.gameMap.ydim;
      this.xpixels = this.ctx.canvas.width;
      this.ypixels = this.ctx.canvas.height;
      this.center_x = (this.ctx.canvas.width / 2.0) | 0;
      this.center_y = (this.ctx.canvas.height / 2.0) | 0;
      _ref = Object.keys(this.options);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        k = _ref[_i];
        this[k] = this.options[k];
      }
      this.ulx = parseInt((this.xpixels - this.xdim * this.dim_to_pix) / 2.0);
      this.uly = parseInt((this.ypixels - this.ydim * this.dim_to_pix) / 2.0);
      this.turn_board = false;
      this.direction = 1;
      console.log("Renderer::@ctx");
    }

    Renderer.prototype.direction = function(direction) {
      this.direction = direction;
    };

    Renderer.prototype.center_on = function(center_on) {
      this.center_on = center_on;
    };

    Renderer.prototype.rotate_board = function(rotate_board) {
      this.rotate_board = rotate_board;
    };

    Renderer.prototype.render_map_to_play_area = function() {
      var dirtype, elemy, xx, yy, _i, _ref, _results;
      dirtype = this.turn_board ? this.direction : 1;
      _results = [];
      for (xx = _i = 0, _ref = this.xdim; 0 <= _ref ? _i < _ref : _i > _ref; xx = 0 <= _ref ? ++_i : --_i) {
        _results.push((function() {
          var _j, _ref1, _results1;
          _results1 = [];
          for (yy = _j = 0, _ref1 = this.ydim; 0 <= _ref1 ? _j < _ref1 : _j > _ref1; yy = 0 <= _ref1 ? ++_j : --_j) {
            elemy = this.gameMap.mapGet(xx, yy, dirtype);
            if (this.center_on === "snake_head") {
              this.xoff = this.center_x + (xx - this.snake.X) * this.dim_to_pix;
              this.yoff = this.center_y + (yy - this.snake.Y) * this.dim_to_pix;
            } else if (this.center_on === "snake_box") {
              this.xoff = this.ulx + xx * this.dim_to_pix;
              this.yoff = this.uly + yy * this.dim_to_pix;
            }
            if (elemy && elemy !== 0) {
              _results1.push(elemy.draw(this.ctx, this.xoff, this.yoff, this.dim_to_pix, this.dim_to_pix));
            } else {
              _results1.push(void 0);
            }
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };

    Renderer.prototype.draw = function() {
      this.direction = this.snake.direction;
      this.ctx.fillStyle = this.palette.game_board_bg;
      this.ctx.clearRect(0, 0, this.xpixels, this.ypixels);
      this.ctx.fillRect(0, 0, this.xpixels, this.ypixels);
      if (this.center_on === "snake_head") {
        if (this.rotate_board) {

        } else {
          this.xoff = this.center_x - (this.snake.X * this.dim_to_pix);
          this.yoff = this.center_y - (this.snake.Y * this.dim_to_pix);
        }
        this.ctx.fillStyle = this.palette.snake_box_bg;
        this.ctx.fillRect(this.xoff + 1, this.yoff + 1, this.xdim * this.dim_to_pix - 1, this.ydim * this.dim_to_pix - 1);
        this.ctx.strokeStyle = this.palette.snake_box_border;
        this.ctx.strokeRect(this.xoff, this.yoff, this.xdim * this.dim_to_pix, this.ydim * this.dim_to_pix);
        this.ctx.strokeStyle = this.palette.snake_head_stroke;
        this.ctx.fillStyle = this.palette.snake_head_color;
        this.ctx.strokeRect(this.center_x, this.center_y, this.dim_to_pix - 1, this.dim_to_pix - 1);
        this.ctx.fillRect(this.center_x, this.center_y, this.dim_to_pix - 1, this.dim_to_pix - 1);
        return this.render_map_to_play_area();
      } else {
        this.ctx.fillStyle = this.palette.snake_box_bg;
        this.ctx.fillRect(this.ulx, this.uly, this.xdim * this.dim_to_pix - 1, this.ydim * this.dim_to_pix - 1);
        this.ctx.strokeStyle = this.palette.snake_box_border;
        this.ctx.strokeRect(this.ulx, this.uly, this.xdim * this.dim_to_pix, this.ydim * this.dim_to_pix);
        this.ctx.strokeStyle = this.palette.snake_head_stroke;
        this.ctx.fillStyle = this.palette.snake_head_color;
        this.ctx.strokeRect(this.ulx + this.snake.X * this.dim_to_pix, this.uly + this.snake.Y * this.dim_to_pix, this.dim_to_pix - 1, this.dim_to_pix - 1);
        this.ctx.fillRect(this.ulx + this.snake.X * this.dim_to_pix, this.uly + this.snake.Y * this.dim_to_pix, this.dim_to_pix - 1, this.dim_to_pix - 1);
        return this.render_map_to_play_area();
      }
    };

    return Renderer;

  })();

  Globals = (function() {

    function Globals() {
      this.element_id_lookup = element_id_lookup;
      window.elt = this.element_id_lookup;
    }

    return Globals;

  })();

  BaseElement = (function() {

    function BaseElement(locx, locy) {
      var _ref, _ref1;
      this.locx = locx;
      this.locy = locy;
      this.id = -1;
      this.value = 0;
      this.ttl = 999999999999;
      this.age = 0;
      if ((_ref = this.locx) == null) {
        this.locx = -1;
      }
      this.name = "basic item";
      if ((_ref1 = this.locy) == null) {
        this.locy = -1;
      }
    }

    BaseElement.prototype.draw = function(ctx, offx, offy, sizex, sizey) {};

    BaseElement.prototype.munched = function(snake) {
      game.score.addMessage(this, "Snake ate a " + this.name);
      return this.name;
    };

    BaseElement.prototype.decay = function() {
      this.ttl -= 1;
      return this.age += 1;
    };

    BaseElement.prototype.update = function() {
      return this.decay();
    };

    return BaseElement;

  })();

  NullElement = (function(_super) {

    __extends(NullElement, _super);

    function NullElement(locx, locy) {
      this.locx = locx;
      this.locy = locy;
      this.id = 0;
      NullElement.__super__.constructor.call(this);
    }

    NullElement.prototype.draw = function(ctx, offx, offy, sizex, sizey) {};

    return NullElement;

  })(BaseElement);

  SnakeBody = (function(_super) {

    __extends(SnakeBody, _super);

    function SnakeBody(locx, locy) {
      this.locx = locx;
      this.locy = locy;
      this.id = elt['snake_body'];
      SnakeBody.__super__.constructor.call(this);
      this.name = "snake body";
    }

    SnakeBody.prototype.draw = function(ctx, offx, offy, sizex, sizey) {
      ctx.fillStyle = game.renderer.palette.snake_body_color;
      return ctx.fillRect(offx, offy, sizex - 1, sizey - 1);
    };

    SnakeBody.prototype.munched = function(snake) {
      return SnakeBody.__super__.munched.apply(this, arguments);
    };

    return SnakeBody;

  })(BaseElement);

  Mushroom = (function(_super) {

    __extends(Mushroom, _super);

    function Mushroom(locx, locy, score, ticks, snake) {
      this.locx = locx;
      this.locy = locy;
      Mushroom.__super__.constructor.call(this);
      this.name = "mushroom";
      this.sat_range = [25, 100];
      this.lum_range = [20, 50];
      this.val_range = [25.0, 100.0];
      this.color_value = this.val_range[0];
      this.basecolor = this.bc = window.Color("#86FFF6");
      this.bc = this.bc.value(this.val_range[0]);
      this.base_value = 25;
      this.id = elt['mushroom'];
    }

    Mushroom.prototype.draw = function(ctx, offx, offy, sizex, sizey) {
      var ts;
      ts = ctx.strokeStyle;
      this.color_value = Math.min(this.color_value * 1.03, 100.0);
      this.bc.value(this.color_value);
      this.value = parseInt(this.base_value * (this.bc.hsvArray()[2] / 100.0));
      ctx.strokeStyle = this.bc.rgbString();
      ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
      return ctx.strokeStyle = ts;
    };

    return Mushroom;

  })(BaseElement);

  Mushroom2 = (function(_super) {

    __extends(Mushroom2, _super);

    function Mushroom2(locx, locy) {
      this.locx = locx;
      this.locy = locy;
      Mushroom2.__super__.constructor.call(this);
      this.value = 25;
    }

    Mushroom2.prototype.draw = function(ctx, offx, offy, sizex, sizey) {
      var ts;
      ts = ctx.strokeStyle;
      ctx.strokeStyle = '#cc11ee';
      ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
      return ctx.strokeStyle = ts;
    };

    return Mushroom2;

  })(BaseElement);

  Mushroom3 = (function(_super) {

    __extends(Mushroom3, _super);

    function Mushroom3(locx, locy) {
      this.locx = locx;
      this.locy = locy;
      Mushroom3.__super__.constructor.call(this);
      this.value = 40;
    }

    Mushroom3.prototype.draw = function(ctx, offx, offy, sizex, sizey) {
      var ts;
      ts = ctx.strokeStyle;
      ctx.strokeStyle = '#bb23a0';
      ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
      return ctx.strokeStyle = ts;
    };

    return Mushroom3;

  })(BaseElement);

  Farmer = (function(_super) {

    __extends(Farmer, _super);

    function Farmer(locx, locy) {
      this.locx = locx;
      this.locy = locy;
      console.log(" new farmer " + this.locx + this.locy);
      Farmer.__super__.constructor.call(this, this.locx, this.locy);
      this.name = "farmer";
      this.basecolor = "#8B6914";
      this.direction = 0;
      this.moving = false;
      this.value = 300;
      this.decide_move();
      this.move_rate = 0.05 + Math.random() * 0.09;
      game.score.addMessage(this, "A Farmer just appeared.");
      this.id = elt['farmer'];
    }

    Farmer.prototype.decide_move = function() {
      this.movedir = (Math.random() * 5 | 0) - 1;
      return console.log("Farmer " + this.id + " has decided to move " + this.movedir);
    };

    Farmer.prototype.move = function(map) {
      var next_square;
      if (this.movedir < 0 && Math.random() > 0.03) {
        return;
      }
      if (this.movedir < 0) {
        this.decide_move();
      }
      if (this.locy > -1 && this.locx > -1 && Math.random() < this.move_rate) {
        next_square = game.gameMap.with_wrap(this.locx + game.snake.xV[this.movedir], this.locy + game.snake.yV[this.movedir]);
        if (map[next_square[0]] && map[next_square[0]][next_square[1]] === 0) {
          map[this.locx][this.locy] = 0;
          this.locx = next_square[0];
          this.locy = next_square[1];
          map[this.locx][this.locy] = this;
        } else {
          this.decide_move();
        }
      }
      return this;
    };

    Farmer.prototype.draw = function(ctx, offx, offy, sizex, sizey) {
      var ts;
      ts = ctx.strokeStyle;
      ctx.strokeStyle = this.basecolor;
      ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
      return ctx.strokeStyle = ts;
    };

    Farmer.prototype.update = function(map) {
      Farmer.__super__.update.apply(this, arguments);
      return this.move(map);
    };

    return Farmer;

  })(BaseElement);

  window.globals = new Globals;

  window.BaseElement = BaseElement;

  window.SnakeBody = SnakeBody;

  window.Mushroom = Mushroom;

  window.GameMap = GameMap;

  window.ColorPalette = ColorPalette;

  window.Renderer = Renderer;

  window.Level = Level;

}).call(this);
