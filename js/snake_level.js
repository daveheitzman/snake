// Generated by CoffeeScript 1.3.3
(function() {
  var level0, level1;

  window.Level = (function() {

    function Level(num) {
      this.number = num || 0;
      console.log('level' + this.number);
      this.messageStr = "default message";
      this.active = false;
      this.preStartComplete = false;
    }

    Level.prototype.message = function() {
      return "Level " + this.number + ": " + this.messageStr;
    };

    Level.prototype.preStart = function(ctx) {
      var h, w,
        _this = this;
      this.ctx = ctx;
      console.log(this.ctx);
      h = this.ctx.canvas.getAttribute("height");
      w = this.ctx.canvas.getAttribute("width");
      this.ctx.clearRect(0, 0, w, h);
      this.ctx.fillStyle = "#222222";
      this.ctx.fillRect(0, 0, w, h);
      this.ctx.fillStyle = "#ffffff";
      this.ctx.fillText(this.message(), 105, 30);
      return setTimeout(function() {
        return _this.preStartComplete = true;
      }, 3000);
    };

    Level.prototype.done = function() {
      return false;
    };

    Level.prototype.beforeRender = function() {};

    Level.prototype.beforeCollide = function() {};

    Level.prototype.afterCollide = function() {};

    Level.prototype.updateMapDwellers = function() {};

    Level.prototype.snakeDie = function() {};

    Level.prototype.afterDone = function() {};

    return Level;

  })();

  level0 = new Level(0);

  level0.beforeCollide = function() {};

  level0.done = function() {
    game.score.score > 50;
    return false;
  };

  level0.messageStr = "This is the only level";

  level1 = new Level(1);

  level1.messageStr = "Eat 25 mushrooms.";

  window.snake_level_set = [level0, level1];

}).call(this);
