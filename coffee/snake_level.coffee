###########################################   Levels   ############################################################
class window.Level
  constructor: (num)->
    #defaults
    @number = num || 0
    console.log('level' + @number) 
    @messageStr = "default message"
    @active = false 
    @preStartComplete=false
  message: () ->
    "Level " + @number + ": " + @messageStr
  preStart: (@ctx)->
    console.log @ctx
    h=@ctx.canvas.getAttribute("height")
    w=@ctx.canvas.getAttribute("width")
    @ctx.clearRect(0,0,w,h);
    @ctx.fillStyle = "#222222" ;
    @ctx.fillRect(0,0,w,h);
    @ctx.fillStyle="#ffffff"
    @ctx.fillText(@message(), 105, 30);
    setTimeout( ()=>
        @preStartComplete = true
      3000)
  done: ()-> 
    # console.log("default Level::done")
    false
  beforeRender: ()->
    # console.log("default Level::beforeRender")
  beforeCollide: ()->
    # console.log("default Level::beforeCollide")
  afterCollide: ()->
    # console.log("default Level::afterCollide")
  updateMapDwellers: ()->
    # console.log("default Level::updateMapDwellers")
  snakeDie: ()->
  afterDone: ()->


# this belongs in a separate file.
level0= new Level(0)
level0.beforeCollide = ()->
level0.done = ()->
  game.score.score > 50
  false
# level0.messageStr = "Get more than 50 points."
level0.messageStr = "This is the only level"
# level0.message = ()->
  
  # console.log(" overridden beforeCollide in level0")

level1 = new Level(1)
level1.messageStr = "Eat 25 mushrooms."
# level0.message = "Level 1"
  
window.snake_level_set=[
  level0
  level1
]
