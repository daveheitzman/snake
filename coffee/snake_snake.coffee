###########################################   THE SNAKE    ##########################################################

class window.Snake
  constructor: (x,y) ->
    @elements=1
    @body_to_mush_index=0
    @queue=[]
    @length=1
    @X=x
    @Y=y
    @turn=[]
    @direction=1 #north
    @xV = [-1, 0, 1, 0]
    @yV = [0, -1, 0, 1]
    @mode = "alive" # "dead" is the other one. there could be others, like "fast", "invincible" etc

  collide: (gameMap) ->
    oldxy=[@X,@Y]
    elem=gameMap.map[@X][@Y]
    # console.log elem
    if elem==0
      @queue.unshift(oldxy)
    else 
      console.log elem
      new_mode = elem.munched(this)
      @mode = "dead" if new_mode == "snake body"
      # window.game.setScore( score+= (Math.max(5, inc_score) + scoreBonus() ) ) ;
      # window.game.inc_score = 50;
      @queue.unshift(oldxy)
        # console.log(@queue)
      @elements++
      game.score.update( elem.value )
    @move()
      
    if @elements < @queue.length 
      erase = @queue.pop()
      game.publishLength()
      gameMap.setSquare(erase,0)

    return oldxy
  
  explode: (gameMap) ->
    seg=@queue[@body_to_mush_index]
    gameMap.map[seg.locx][seg.locy] = new Mushroom
    @body_to_mush_index += 1 unless @body_to_mush_index >= (@queue.length -1 )

  move: (newDir)->
    # console.log @queue
    if @turn.length > 0  
      dir = @turn.pop();
      if  (dir % 2) != (@direction % 2)
        @direction = dir
    new_loc=game.gameMap.with_wrap(@X,@Y)
    # console.log new_loc
    @X += @xV[@direction]
    @Y += @yV[@direction]

    # if @elements > 10
      # this slices off the end of the snake leaving body strewn on board where he can hit it again
      # @queue=@queue.slice(0,1)
      # @elements=1   
