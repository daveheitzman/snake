#############################################   Controller   #####################################################
class window.Controller
  constructor: (@rndrr, @snake_level_set, @score  ) ->
    window.document.onkeydown = @keyListener 
    @score.currentLevelNum = -1
    @currentLevel =  @snake_level_set[@currentLevelNum]
    @interval=null
    @sum = 0
    @ticks = 0
    @easy = true 
    @snake=@rndrr.snake
    @xdim = @rndrr.xdim
    @ydim = @rndrr.ydim
    @map = @rndrr.gameMap.map   
    @gameMap = @rndrr.gameMap   
    console.log("Controller::@snake_level_set")
    console.log(@snake_level_set)
    @nextLevel() 
    # @interval=setInterval(@gameLoop, 120)
  
  keyListener: (e) =>
    console.log("controller.keyListener")
    console.log(e)
    code = e.keyCode - 37
        #  /*
        #   * 0: left
        #   * 1: up
        #   * 2: right
        #   * 3: down
        #   **/
    if window.game.control_type == "absolute" && (0 <= code && code < 4 && code != @snake.turn[0]) 
      e.preventDefault()
      @snake.turn.unshift(code)
    else if window.game.control_type != "absolute" && 0 == code || 2 == code &&  code != @snake.turn[0] 
      e.preventDefault()
      if code == 0
        newdir=@snake.direction - 1
      else if code == 2
        newdir=@snake.direction + 1
      newdir=(newdir + 4) % 4
      console.log('newdir ' + newdir )
      @snake.turn.unshift(newdir)
    else if (-5 == code) 
      if (@interval) 
        clearInterval(@interval)
        @interval = 0
      else
        @interval = setInterval(@gameLoop, 120)
    else
      dir = @sum + code
      if (dir == 44||dir==94||dir==126||dir==171) 
        @sum += code
      else if (dir == 218) 
        @easy = 1

  nextLevel: () ->
    @score.currentLevelNum += 1
    @currentLevel = @snake_level_set[@score.currentLevelNum]
    # @currentLevel = level
    clearInterval(@interval)
    @currentLevel.preStart(@rndrr.ctx)
    clearInterval(@interval)
    @interval = setInterval(@gameLoop, 120)

  gameLoop: () => #the fat arrow means that when this function is used as a lambda by setInterval, this will still refer to the controller object 
    # console.log("controller.gameLoop")
    # console.log('control_type');
    # console.log(window.game.control_type);
    @ticks += 1 
    game.ticks = @ticks
    snake = @snake
    @easy=true #; // mainly for debugging
    @score.draw()
    if @currentLevel.done() 
      @currentLevel.afterDone()
      @nextLevel()
    else if !@currentLevel.preStartComplete 
    else if snake.mode == "alive" 
      if @easy
        snake.X = (snake.X + @xdim) % @xdim
        snake.Y = (snake.Y + @ydim) % @ydim
      game.score.dec_bonus()
      if (@easy || (0 <= snake.X &&  snake.X < @xdim && 0 <= snake.Y && snake.Y < @ydim)) && 2 != @map[snake.X][snake.Y].id
        #the levels will eventually handle all this.
        # if (Math.random() < 0.1)
        #   gameMap.placeFood()
        @currentLevel.beforeRender()
        # $("#debug").html(direction+","+snake.X+" : "+snake.Y)
          # //clear the canvas
        $("#debug").html( $("input#follow_head").attr('checked') )
        if ( $("input#follow_head").attr('checked') == "checked" )
          @rndrr.center_on="snake_head"
        else
          @rndrr.center_on="snake_box"
        if ( $("input#turn_board").attr('checked') =="checked" )
          @rndrr.turn_board=true
        else
          @rndrr.turn_board=false
  #  update position
        @rndrr.draw()
        @currentLevel.beforeCollide()
        oldxy=snake.collide(gameMap);
        # @map[ oldxy[0] ][ oldxy[1] ] = new SnakeBody;
        # @map[ 5][ 5 ] = new SnakeBody
        @gameMap.putSnakeOn(oldxy)
        # console.log(@map.map == @map)
        @currentLevel.afterCollide()
        @gameMap.updateMapDwellers(@score,  @ticks , @snake )
    else if snake.mode=="dead"
      snake.mode = "exploding"
      alert ("dead!")
    # else if snake.mode=="exploding"
