class window.Score #the whole scoreboard
  constructor:(@ctx) ->
    @level = new Level
    @currentLevelNum = 0
    @score = 0
    @inc_score=50
    @reset()
    @last_score_eaten= 0
    @last_time_bonus = 0
    @time_bonus=50
    # @draw()
  reset: ()->
    @time_bonus=50
  dec_bonus: ()->
    @time_bonus-- unless @time_bonus < 6
    @draw()  
  update: (inc_s) ->
    # console.log inc_s
    @last_score_eaten = inc_s |0
    @last_time_bonus = @time_bonus
    @score += @time_bonus 
    @score += @last_score_eaten 
    @reset()
    @draw()

  addMessage: (@who, @message)->
    @who = @message = '' unless @who? && @message?     
  scoreBonus: ()->
    pct= game.snake.elements/ (game.gameMap.xdim * game.gameMap.ydim * 1.0)
    degree = 1/(1-pct); # this will grow higher the longer he gets, in a non-linear way.
    # console.log('score bonus');
    # console.log(parseInt(degree*0.500));
    parseInt(degree*0.500)
    # // other ideas..
    # // how curled up the snake is? 
    # // is he currently trapped? 

  # write_this: (nctx)->
  #   h = nctx.height
  #   w = nctx.width
  #   # nctx.clearRect(0,0,w,h)
  #   nctx.clearRect(0,0,w,h)
  #   nctx.fillStyle="#006600"
  #   nctx.fillRect(0,0,w,h)
  #   nctx.fillStyle="#efefef"
  #   s="Score: "+game.score.score+" Bonus: "+game.score.inc_score
  #   nctx.fillText(s, 105, 30);


  draw: () ->
    # console.log('score::draw')
    # console.log(@score)
    # console.log(@inc_score)

    # @ctx=window.game.ctx_score

    # h=@ctx.canvas.height;
    # w=@ctx.canvas.width;
    # @ctx.clearRect(0,0,w,h);
    # @ctx.fillStyle = "#222222" ;
    # @ctx.fillRect(0,0,w,h);
    # @ctx.fillStyle="#ffffff"
    # @ctx.fillText("asdf;ljk", 105, 30);

    # for whatever reason, cannot access the ctx_score (the canvas device context for the scoreboard, from here.)
    # ctx=game.ctx_score
    # ctx.clearRect(0,0,500,100)
    # ctx.fillStyle=game.palette.score.fill
    # console.log game.ctx_score
    # game.ctx_score.fillStyle="#ffffff"
    # game.ctx_score.clearRect(0,0,500,100)
    # game.ctx_score.fillRect(0,0,500,100)
    # game.scoreboard_with_function(@write_this)

    game.write_to_scoreboard(["Level: " + @currentLevelNum + " Score: "+@score+" Bonus: "+@inc_score+ " Last eaten: "+@last_score_eaten + " Last time bonus: "+ @last_time_bonus, @message]);

    # ctx_info=$("div#game_right div#game_info canvas")
    # ctx_info=$("div#game_right div#game_info canvas").show();
    # ctx_info=window.game.ctx_info
    # # console.log ctx_info
    # h=ctx_info.canvas.height
    # w=ctx_info.canvas.width
    # s= "ttttttttt"
    # ctx_info.clearRect(0,0,w,h)
    # ctx_info.fillStyle = "#33aad2" 
    # ctx_info.fillRect(0,0,w,h)
    # ctx_info.fillStyle="#ffffff"
    # ctx_info.fillText(s, 105, 30)


    # @ctx.clearRect(0,0,500,100)
    # @ctx.fillRect(0,0,500,100)
    # @ctx.fillStyle="#ffffff"
    # console.log @ctx
    # @ctx.fillText("Score: "+@score, 5, 30);
    # @ctx.fillStyle="#d21f23"
