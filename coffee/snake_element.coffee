

# snake_level_set = require 'snake_level_set'
# ids: 
window.elt=element_id_lookup = {

  snake_body : 2
  2: () -> new SnakeBody

  mushroom : 15
  15 : () -> new Mushroom

  mushroom2 : 16
  16 : () -> new Mushroom2
  
  farmer : 30
  30 : () -> new Farmer
  
  0: () -> new NullElement
  null_element: 0
}

###########################################   DWELLERS -- manages the things that live on the map ###################
class MapDwellers 
  constructor: () ->
    #all the following need to be subclases of BasicElement
    #the array is filled with a bunch of [x,y]. gameMap[x,y] gives you the actual object
    # gameMap.mapGet(@treats[0]) returns the object, which might be a 0, sorry. 
    # @treats = [] #non moving food type items
    # @creatures=[]
    # @blocks=[]
    # @teleports=[]
    # @snake_carcass = [] # hunks of snake that are detached from the old boy, but dangerous to run into 
    @dwellers = [] # everything on the map that is not the snake's body, but in a linear array 
    @farmers = [] 
    @newFoodRate = 0.04 # 0.0 < 1.0
    @farmerSpawnRate = 0.002
  update: (gameMap, score, ticks, snake) ->
    # console.log 'update map dwellers()'
    # console.log 'MapDwellers.update()'
    #cull elements if they've been removed from the map
    new_dwellers=[]
    #cull any from the list who have been set to 0 on the map, and then call update() on the remaining live ones
    # console.log @dwellers
    for dw in @dwellers
      if dw.locx < 0 || dw.locy < 0 || gameMap.map[dw.locx][dw.locy] != 0
        new_dwellers.push(dw)
        dw.update(gameMap.map)
    @dwellers = new_dwellers

    mush=gameMap.placeFood( score, ticks, snake ) if Math.random() <= @newFoodRate
    farmer=gameMap.placeFarmer( score, ticks, snake ) if Math.random() <= @farmerSpawnRate
    # console.log mush 
    # console.log mush? 
    if mush?
      @dwellers.push mush
    if farmer?
      @dwellers.push farmer


    # console.log @dwellers 
    # @newFoodRate = 
    map=gameMap
    sn=game.snake
  



###########################################   Game Map   ############################################################
class GameMap
  # console.log('GameMap:constructor')
  constructor: (@xdim,@ydim, @dim_to_pix) ->
    @map=[]
    for co in [0...@xdim] 
      @map[co] =[]
      for co2 in [0...@ydim]
        @map[co][co2] = 0 #new NullElement
    @mapDwellers=new MapDwellers(this)

  mapGet: (x,y,dir)->
    # this yields the element on the map taking into account the current map rotation
    # console.log typeof(x)
    # if typeof x == "object"
    xtmp=x 
    ytmp=y
    if x[0]
      xtmp=x[0]
      ytmp=x[1]
    x=xtmp
    y=ytmp
    return null if 0 > x > @xdim-1 || 0 > y > @ydim - 1 
    dir ?=  1
    switch dir
      when 0 # west/left  - map rot clockwise 90 deg
        @map[ y ][ @xdim-1-x ]
      when 1  # north
        # console.log @map
        # if x < 2  
        # console.log x+","+y
        @map[x][y]
      when 2 #south/down
        @map[ @xdim-1-x ][ @ydim-1-y ]
      when 3 #east/right - map rot counter clockwise 90 deg
        @map[ @xdim-1-y ][ x ]

  updateMapDwellers: (score,ticks,snake)->
    # console.log "GameMap.updateMapDwellers()"
    @mapDwellers.update(this,score,ticks,snake)

  placeFood: ( score, ticks, snake )->
    # console.log "placeFood()"
    re=@randomEmpty()
    if re? 
      @map[ re[0] ][ re[1] ] = new Mushroom(re[0],re[1],score, ticks, snake)
    else
      null    

  placeFarmer:  (score,ticks,snake)->
    console.log("GameMap.placeFarmer() ")
    re=@randomEmpty()
    if re? 
      @map[re[0]][re[1]] = new Farmer(re[0],re[1],score, ticks, snake)
    else
      null    
  
  randomEmpty: ()->   
    MR=Math.random
    x = MR() * @xdim|0
    y = MR() * @ydim|0
    failsafe=25
    while @map[x][y] != 0 && failsafe > 0 
      x = MR() * @xdim|0
      y = MR() * @ydim|0
      failsafe--
      0 # so it doesn't push new objects onto the return stack       
    if @map[x][y] == 0
      [x,y]
    else
      null

  putSnakeOn: (oldxy) ->
    # @map[ oldxy[0] ][ oldxy[1] ]=new SnakeBody
    @setSquare oldxy, (new SnakeBody)

  setSquare: (erase,v) ->
    @map[ erase[0] ][ erase[1] ] = v

  empties_orth: (x,y) ->
    #returns the empty squares (only N,S,E,W) surrounding a given x,y coordinates
    empties = [] 
    return empties unless x>-1 && y>-1 && x<@xdim && y<@ydim
    empties.push([x,y-1]) if map[x][y-1]==0 #N
    empties.push([x+1,y]) if map[x+1][y]==0 #E
    empties.push([x,y+1]) if map[x][y+1]==0 #S
    empties.push([x-1,y]) if map[x-1][y]==0 #W
    empties
  empties_diag: (x,y) ->
    #returns the empty squares (only NW,SW,SE,NE) surrounding a given x,y coordinates
    empties = [] 
    return empties unless x>-1 && y>-1 && x<@xdim && y<@ydim
    empties.push([x-1,y-1]) if map[x-1][y-1]==0 #NW
    empties.push([x+1,y-1]) if map[x+1][y-1]==0 #NE
    empties.push([x+1,y+1]) if map[x+1][y+1]==0 #SE
    empties.push([x-1,y+1]) if map[x-1][y+1]==0 #SW
    empties
  with_wrap: (x,y)->
    [(@xdim + (x % (@xdim))) % @xdim,(@ydim + (y % (@ydim))) % @ydim]

class ColorPalette
  # console.log('ColorPalette:constructor')
  constructor: () ->
    @snake_box_bg="#113311"
    @snake_box_border="#337723"
    @game_board_bg ="#000000"
    @score_bg = "#000000"
    @snake_body_color ="#aaddaa"
    @snake_head_color ="#ccffcc"
    @snake_head_stroke ="#eeffee"
    @score=
      stroke: "#ee2223"
      fill: "#000000"



###########################################   RENDERER -- renders your map according to rotation, center ############
class Renderer
  constructor: (@ctx,@gameMap,@snake,@options) ->
    #options={colorPalette: a color palette, center_on: center_on, rotate_board: t/f}
    console.log('Renderer:constructor')
    console.log('ctx:')
    console.log(ctx)
    @center_on = "snake_box"
    @dim_to_pix=@gameMap.dim_to_pix
    @xdim = @gameMap.xdim
    @ydim = @gameMap.ydim
    @xpixels=@ctx.canvas.width
    @ypixels=@ctx.canvas.height
    @center_x = (@ctx.canvas.width / 2.0)|0
    @center_y = (@ctx.canvas.height / 2.0)|0
    for k in Object.keys(@options) 
      this[k] = @options[k]
    #this is for when we are centered on snake_box so the bg doesn't move around. it is the ul corner of the bg box
    @ulx=parseInt( (@xpixels-@xdim*@dim_to_pix) / 2.0 )  
    @uly=parseInt( (@ypixels-@ydim*@dim_to_pix) / 2.0 )
    @turn_board=false
    @direction=1
    # console.log("Renderer::@snake")
    console.log("Renderer::@ctx")
    # console.log(@snake)

  direction: (@direction) ->
  center_on: (@center_on) ->

  rotate_board: (@rotate_board) ->    
  render_map_to_play_area: () ->
    # console.log('Renderer::render_map_to_play_area()')
    # console.log(@xdim, @ydim)
    dirtype= if @turn_board then @direction else 1
    for xx in [0...@xdim] 
      for yy in [0...@ydim]
        elemy = @gameMap.mapGet(xx,yy,dirtype) # //1 forces 'north'
        # console.log(elemy)
        if @center_on == "snake_head" 
          @xoff = @center_x + (xx - @snake.X)*@dim_to_pix
          @yoff = @center_y + (yy - @snake.Y)*@dim_to_pix
        else if @center_on == "snake_box" 
          @xoff = @ulx + (xx)*@dim_to_pix
          @yoff = @uly + (yy)*@dim_to_pix

        if elemy && elemy != 0
          # console.log(@ctx, @xoff , @yoff , @dim_to_pix , @dim_to_pix)
          elemy.draw @ctx, @xoff , @yoff , @dim_to_pix , @dim_to_pix 

  draw: () ->
    # @ctx=window.ctx_main
    # console.log(@ctx)

    # console.log('Renderer::draw()')
    # console.log(@xpixels)
    # console.log(@ypixels)
    # console.log(@palette)
    @direction = @snake.direction
    @ctx.fillStyle = @palette.game_board_bg;
    @ctx.clearRect(0,0,@xpixels, @ypixels);
    @ctx.fillRect(0,0,@xpixels, @ypixels);

    if @center_on=="snake_head"
      # 0=left, 1= up, 2=right, 3=down

      # go through map and draw everything relative to the centered snake head
      if (@rotate_board) 
      else
        @xoff=@center_x - ( @snake.X*@dim_to_pix )
        @yoff=@center_y - ( @snake.Y*@dim_to_pix )
    #game board 
      @ctx.fillStyle = @palette.snake_box_bg
      @ctx.fillRect(@xoff+1, @yoff+1, @xdim*@dim_to_pix-1, @ydim*@dim_to_pix-1 )
    # and border                 
      @ctx.strokeStyle = @palette.snake_box_border 
      @ctx.strokeRect(@xoff, @yoff, @xdim*@dim_to_pix, @ydim*@dim_to_pix )
    #the always drawn head
      @ctx.strokeStyle = @palette.snake_head_stroke; 
      @ctx.fillStyle = @palette.snake_head_color; 
      @ctx.strokeRect(@center_x , @center_y , @dim_to_pix - 1, @dim_to_pix - 1);
      @ctx.fillRect(@center_x, @center_y , @dim_to_pix - 1, @dim_to_pix - 1);
      @render_map_to_play_area();
    else 
      # console.log('center_on != snake_head')
      @ctx.fillStyle = @palette.snake_box_bg;
      @ctx.fillRect(@ulx , @uly , @xdim*@dim_to_pix-1, @ydim*@dim_to_pix-1 );
      #and border                 
      @ctx.strokeStyle = @palette.snake_box_border; 
      @ctx.strokeRect(@ulx, @uly, @xdim*@dim_to_pix, @ydim*@dim_to_pix );
      
      #snake head
      @ctx.strokeStyle = @palette.snake_head_stroke
      @ctx.fillStyle = @palette.snake_head_color
      @ctx.strokeRect( @ulx + (@snake.X)*@dim_to_pix ,  @uly + (@snake.Y)*@dim_to_pix , @dim_to_pix - 1, @dim_to_pix - 1)
      @ctx.fillRect(@ulx + (@snake.X)*@dim_to_pix, @uly + (@snake.Y)*@dim_to_pix , @dim_to_pix - 1, @dim_to_pix - 1)
      @render_map_to_play_area()
  





############################################# munchable or collidable elements #####################################


class Globals
  constructor: () ->
    @element_id_lookup = element_id_lookup
    window.elt=@element_id_lookup;

class BaseElement
  constructor: (@locx,@locy) ->
    @id=-1
    @value = 0
    @ttl = 999999999999
    @age = 0
    @locx ?= -1
    @name = "basic item"
    @locy ?= -1

  draw: (ctx, offx, offy, sizex, sizey) ->
  munched: (snake) ->
    game.score.addMessage this, "Snake ate a #{@name}"
    @name
    # when the snake eats the thing
  decay: ()-> #a frame has passed. maybe you get darker as time goes by? 
    @ttl -= 1
    @age += 1

  update: ()->
    @decay() 

class NullElement extends BaseElement
  constructor: (@locx,@locy) ->
    @id=0
    super()
  draw: (ctx, offx, offy, sizex, sizey) ->

class SnakeBody extends BaseElement
  constructor: (@locx,@locy) ->
    @id=elt['snake_body']
    super()
    @name = "snake body"
  draw: (ctx, offx, offy, sizex, sizey) ->
    ctx.fillStyle = game.renderer.palette.snake_body_color;
    ctx.fillRect(offx, offy , sizex - 1, sizey - 1 );
  munched: (snake) ->
    super 

class Mushroom extends BaseElement
  # ctx- canvas elements's 2d context
  # offx, offy, where on the canvas to start drawing
  # sizex, sizey - limit of drawing area
  constructor: (@locx,@locy,score, ticks, snake) ->
    # console.log "Mushroom.constructor() score: "+score+" ticks: "+ticks+" snake: "+snake
    # based on score, ticks, snake it determines its color,value and ttl
    super()
    @name = "mushroom"
    @sat_range = [ 25 , 100 ]
    @lum_range = [ 20 , 50 ]
    @val_range = [ 25.0 , 100.0 ]
    @color_value=@val_range[0]
    @basecolor = @bc = window.Color( "#86FFF6" ) #.lighten(@lum_range[0]).saturate(@sat_range[0])
    @bc = @bc.value( @val_range[0] )
    # console.log "hsvArray() " + @bc.hsvArray(); # "rgb(#{@bc[0]},#{@bc[1]},#{@bc[2]})"
    @base_value = 25 #points when eaten, not color
    @id=elt['mushroom']

  draw: (ctx, offx, offy, sizex, sizey) ->
    # console.log('mushroom.draw')
    ts=ctx.strokeStyle ;
    @color_value = Math.min(@color_value*1.03,100.0)
    @bc.value(@color_value)
    @value =  parseInt( @base_value*(@bc.hsvArray()[2] / 100.0 ) )
    ctx.strokeStyle = @bc.rgbString(); # "rgb(#{@bc[0]},#{@bc[1]},#{@bc[2]})"
    ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
    ctx.strokeStyle = ts;


class Mushroom2 extends BaseElement
  # ctx- canvas elements's 2d context
  # offx, offy, where on the canvas to start drawing
  # sizex, sizey - limit of drawing area
  constructor: (@locx,@locy) ->
    # @id=elt['mushroom']
    super()
    @value = 25
  draw: (ctx, offx, offy, sizex, sizey) ->
    ts=ctx.strokeStyle ;
    ctx.strokeStyle = '#cc11ee' ;
    ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
    ctx.strokeStyle = ts;

class Mushroom3 extends BaseElement
  # ctx- canvas elements's 2d context
  # offx, offy, where on the canvas to start drawing
  # sizex, sizey - limit of drawing area
  constructor: (@locx,@locy) ->
    # @id=elt['mushroom']
    super()
    @value = 40
  draw: (ctx, offx, offy, sizex, sizey) ->
    ts=ctx.strokeStyle ;
    ctx.strokeStyle = '#bb23a0' ;
    ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
    ctx.strokeStyle = ts;

class Farmer extends BaseElement
  # ctx- canvas elements's 2d context
  # offx, offy, where on the canvas to start drawing
  # sizex, sizey - limit of drawing area
  # Farmer - he moves around leaving behind tasty mushrooms. but he may be worth eating also ! 
  constructor: (@locx,@locy) ->
    console.log(" new farmer " + @locx + @locy)
    # @id=elt['mushroom']
    super(@locx,@locy)
    @name = "farmer"
    @basecolor="#8B6914"
    @direction = 0
    @moving = false
    @value = 300
    @decide_move()
    @move_rate = 0.05 + Math.random()*0.09 #just a 
    game.score.addMessage(this, "A Farmer just appeared.")
    @id = elt['farmer']

  decide_move:()->
    @movedir = (Math.random()*5 | 0) - 1 #(-1..3)
    console.log "Farmer #{@id} has decided to move #{@movedir}"  
    #if @movedir is -1 we're not moving.
    # empties = map.empties_orth(@locx,@locy)
    # if empties.length > 0

  move: (map)->
    return if @movedir < 0 && Math.random() > 0.03 
    @decide_move() if @movedir < 0 
    # console.log("move farmer - #{@locx} #{@locy} ")
    if @locy > -1 && @locx > -1 &&  Math.random() < @move_rate
      # console.log("move farmer - inside loop ")
      # next_square = [ @locx + game.snake.xV[@movedir] , @locy + game.snake.yV[@movedir] ]
      next_square = game.gameMap.with_wrap( @locx + game.snake.xV[@movedir] , @locy + game.snake.yV[@movedir] )

      if map[next_square[0]] && map[next_square[0]][next_square[1]] == 0
        map[@locx][@locy] = 0
        @locx = next_square[0]
        @locy = next_square[1]
        map[@locx][@locy] = this
      else
        @decide_move()
    this  
  draw: (ctx, offx, offy, sizex, sizey) ->
    ts=ctx.strokeStyle ;
    ctx.strokeStyle = @basecolor
    ctx.strokeRect(offx + 1, offy + 1, sizex - 2, sizey - 2);
    ctx.strokeStyle = ts;
  update: (map)->
    super
    @move(map)

#exporting our classes for use in the main js file
window.globals= new Globals
window.BaseElement=BaseElement
window.SnakeBody = SnakeBody
window.Mushroom = Mushroom
# window.Snake = Snake
window.GameMap = GameMap
window.ColorPalette = ColorPalette
window.Renderer = Renderer
# window.Score = Score
# window.Controller = Controller
window.Level = Level


